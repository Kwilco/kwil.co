# kwil.co

Ansible config for the `kwil.co` server.

## Notes

* Caddy fronts all incoming web traffic and routes it appropriately.
* Server dynamic DNS is handled by the `do_dns` role.
