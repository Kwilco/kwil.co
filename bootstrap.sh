#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail
set -o xtrace

sudo apt update
sudo apt upgrade

mkdir -p ~/.ssh
cat id_rsa.pub > ~/.ssh/authorized_keys
sudo bash -c 'echo "PasswordAuthentication no" >> /etc/ssh/sshd_config'

sudo service ssh reload

# Ansible rereqs
sudo apt install --yes python
