#!/bin/bash

set -o errexit

export ANSIBLE_RETRY_FILES_ENABLED=0
ansible-playbook kwil.co.yml \
  --become --ask-become-pass \
  --verbose \
  --inventory-file hosts.ini \
  --vault-password-file=.vault_pass.txt \
  $*
