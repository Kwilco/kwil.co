#Load a world and automatically start the server.
world=/config/1419.wld

#Creates a new world if none is found. World size is specified by: 1(small), 2(medium), and 3(large).
autocreate=1

#Disable uPNP
upnp=0

#Sets the name of the world when using autocreate
worldname=1419

#Sets the difficulty of the world when using autocreate 0(classic), 1(expert), 2(master), 3(journey)
difficulty=4

#Sets the max number of players allowed on a server.  Value must be between 1 and 255
maxplayers=8

#Set the port number
port=1419

#Sets the folder where world files will be stored
worldpath=/config

#Default system priority 0:Realtime, 1:High, 2:AboveNormal, 3:Normal, 4:BelowNormal, 5:Idle
priority=1

#Journey mode power permissions for every individual power. 0: Locked for everyone, 1: Can only be changed by host, 2: Can be changed by everyone
journeypermission_godmode=0
#journeypermission_time_setfrozen=2
#journeypermission_time_setdawn=2
#journeypermission_time_setnoon=2
#journeypermission_time_setdusk=2
#journeypermission_time_setmidnight=2
#journeypermission_wind_setstrength=2
#journeypermission_rain_setstrength=2
#journeypermission_time_setspeed=2
#journeypermission_rain_setfrozen=2
#journeypermission_wind_setfrozen=2
#journeypermission_increaseplacementrange=2
#journeypermission_setdifficulty=2
#journeypermission_biomespread_setfrozen=2
#journeypermission_setspawnrate=2
